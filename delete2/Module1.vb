﻿Module Module1

    Sub Main()

		Test(0)
		Test(1)
		Test(-1)
		Console.ReadLine()

	End Sub


	Function Test(ByVal I As Integer) As String
		Static Q As New Queue(Of Integer)
		If I = 0 Then Q.Enqueue(0)
		If I = 1 Then Q.Enqueue(1)
		If I = -1 Then
			Console.WriteLine(Q.Count)
			Q.Dequeue()
			Console.WriteLine(Q.Count)
		End If
		Return String.Empty
	End Function

End Module
