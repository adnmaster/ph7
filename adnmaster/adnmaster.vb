﻿Imports Newtonsoft.Json
Imports By.Map

Namespace adnmaster.[Default]
	Public Module VB

		Sub Main()
			Main(Function() String.Empty)
		End Sub
		Function Main(ByVal Exe As Func(Of String)) As String
			Return Exe()
		End Function

		Function Enter_NET(ByVal Input As String) As String

			Dim Req As Map.Request = JsonConvert.DeserializeObject(Of Map.Request)(Input)
			Dim Fields As List(Of adnmasterImageFormat2) = JsonConvert.DeserializeObject(Of List(Of adnmasterImageFormat2))(Req.Data)

			If Req.Module.Equals("ConvertImage", StringComparison.Ordinal) Then
				Return (tjanczuk.Edge.Main(Req.Data)) ' Return Main(tjanczuk.Edge.SelectMethod(Req.Data, Fields))
			End If

			Return "..."

		End Function

	End Module
End Namespace