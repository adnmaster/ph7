﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Net
Imports Newtonsoft.Json
Imports By.Map

Namespace tjanczuk

	Public Class Edge

		Public Shared Function SelectMethod(ByVal FieldsA As List(Of adnmasterImageFormat), ByVal AFields As List(Of adnmasterImageFormat2)) As Func(Of String)
			Dim Result As Func(Of String) = Function() "String.Empty"
			If FieldsA IsNot Nothing Then
				Dim Copy As New List(Of adnmasterImageFormat)
				FieldsA.ForEach(Sub(F) Copy.Add(DirectCast(F, adnmasterImageFormat)))
				Result = Function() ConvertImageToLocal(Copy(0).Source, Copy(0).ImageFormat, Copy(0).Destination)
			End If
			If AFields IsNot Nothing Then
				Dim Copy As New List(Of adnmasterImageFormat2)
				AFields.ForEach(Sub(F) Copy.Add(DirectCast(F, adnmasterImageFormat2)))
				Result = Function() DownloadImageToLocal(Copy(0).Source, Copy(0).ImageFormat, Copy(0).Destination)
			End If

			Return Result
		End Function

		' Main()
		Public Shared Function Main(ByVal Data As String) As String
			Dim FieldsA As List(Of adnmasterImageFormat) = Nothing
			Dim AFields As List(Of adnmasterImageFormat2) = Nothing
			Try
				FieldsA = JsonConvert.DeserializeObject(Of List(Of adnmasterImageFormat))(Data)
			Catch ex As Exception
				FieldsA = Nothing
			End Try
			Try
				AFields = JsonConvert.DeserializeObject(Of List(Of adnmasterImageFormat2))(Data)
			Catch ex As Exception
				FieldsA = Nothing
			End Try
			Return SelectMethod(FieldsA, AFields)()
		End Function

		Public Shared ConvertImageToLocal As Func(Of String, ImageFormat, String, String) =
		 Function(Source As String, ToNewFormat As ImageFormat, ToDestination As String) As String
			 Using Im As Image = Image.FromFile(Source)
				 Im.Save(ToDestination, ToNewFormat)
			 End Using
			 Return "OK"
		 End Function

		Public Shared DownloadImageToLocal As Func(Of Uri, ImageFormat, String, String) =
		Function(Source As Uri, ToNewFormat As ImageFormat, ToDestination As String) As String
			Dim WC As New WebClient()
			Using ImgStream As New IO.MemoryStream(WC.DownloadData(Source))
				Using Im As Image = Image.FromStream(ImgStream)
					ImgStream.Flush()
					Im.Save(ToDestination, ToNewFormat)
				End Using
			End Using
			Return "OK"
		End Function

	End Class

End Namespace
