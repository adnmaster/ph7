﻿Imports System.Drawing.Imaging
Imports System.ComponentModel

<EditorBrowsable(EditorBrowsableState.Advanced)>
Public Class Map
	Public Class Request
		Property [Module] As String
		Property Data As String
	End Class
	Public Class adnmasterImageFormat
		Property TupleItem As String
		Property Source As String
		Property ImageFormat As ImageFormat
		Property Destination As String
	End Class
	Public Class adnmasterImageFormat2
		Property TupleItem As String
		Property Source As Uri
		Property ImageFormat As ImageFormat
		Property Destination As String
	End Class

	Public Shared adnmaster_ImageFormat As New Tuple(Of _
	 String, String, String)("Source As String", "ToNewFormat As ImageFormat", "ToDestination As String") '<==
	'"adnmaster_ImageFormat" || "Source As String" || "Source" || "SourceAsString" || "SourceAs String" || "Source AsString" || "Source_As_String" ...

	Public Shared adnmaster_ImageFormat2 As New Tuple(Of _
	 String, String, String)("Source As Uri", "ToNewFormat As ImageFormat", "ToDestination As String") '<==
	'"adnmaster_ImageFormat2" || "Source As Uri"

End Class
