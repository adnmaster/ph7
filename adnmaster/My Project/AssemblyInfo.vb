﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("adnmaster")> 
<Assembly: AssemblyDescription("Hello")> 
<Assembly: AssemblyCompany("Without words")> 
<Assembly: AssemblyProduct("adnmaster")> 
<Assembly: AssemblyCopyright("Public")> 
<Assembly: AssemblyTrademark("Free Market")> 

<Assembly: ComVisible(False)> 

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("14dcf8eb-2042-4acb-b959-d933b99f297d")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("en-US")> 